using System;
using com.ats.mvc.Implementation;
using com.ats.mvc.Injection;
using com.ats.mvc.Injection.Sources;
using UnityEngine;

namespace com.ats.mvc.Implementation
{
    public abstract class AbstractMainContext : MonoBehaviour, IContext, IMonoInjectable, IMonoInitialize
    {
        public bool InitializeOnAwake = true;
        private readonly BindingsContainer _container = new BindingsContainer();
        private InjectablePool _pool = null;
        
        private void Awake()
        {
            if (InitializeOnAwake)
            {
                InitializeContext();
            }
        }

        public abstract void InstallBindings(IBinder binder);

        public void InitializeContext()
        {
            _pool = new InjectablePool(_container);
            _container.Bind<IInjectablePool>().FromInstance(_pool);
            InstallBindings(_container);
            _container.InjectDependencies(this);
            _container.Initialize(this);
        }

        public void Inject(IInjectionContainer binder)
        {
        }

        public Transform InjectionRoot => transform;
        public abstract void Initialize();
    }
}
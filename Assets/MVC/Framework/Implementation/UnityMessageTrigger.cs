using com.ats.mvc.Injection;
using UnityEngine;

namespace com.ats.mvc.Implementation
{
    public class UnityMessageTrigger : MonoBehaviour, ITrigger, IInjectable, IInitialize
    {
        private bool _didFirstEnable;
        public IMediator Mediator { get; private set; }

        public void Initialize()
        {
            Mediator.OnCreated();
            if (!_didFirstEnable)
            {
                Mediator.OnEnabled();
                _didFirstEnable = true;
            }
        }

        private void OnEnable()
        {
            if (Mediator != null && !_didFirstEnable)
            {
                Mediator.OnEnabled();
                _didFirstEnable = true;
            }
        }

        private void OnDisable()
        {
            Mediator.OnDisabled();
            _didFirstEnable = false;
        }

        private void OnDestroy()
        {
            Mediator.OnDestroyed();
        }

        public void Inject(IInjectionContainer binder)
        {
            Mediator = binder.Get<IMediator,UnityMessageTrigger>(this);
        }
    }
}
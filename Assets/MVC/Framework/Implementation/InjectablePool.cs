using System.Collections.Generic;
using System.Linq;
using com.ats.mvc.Injection;
using UnityEngine;

namespace com.ats.mvc.Implementation
{
    class InjectablePool : IInjectablePool
    {
        private readonly Dictionary<GameObject,Queue<GameObject>> _pool 
            = new Dictionary<GameObject, Queue<GameObject>>();

        private Transform _poolTransform = null;
        private Transform PoolTransform
        {
            get
            {
                if (_poolTransform == null)
                {
                    _poolTransform = new GameObject("Pool").transform;
                    Object.DontDestroyOnLoad(_poolTransform.gameObject);
                }
                return _poolTransform;
            }
        }

        private readonly IInjectionContainer _injectionContainer;

        public InjectablePool(IInjectionContainer injectionContainer)
        {
            _injectionContainer = injectionContainer;
        }

        public GameObject GetPoolObject(GameObject prefab, Transform parent)
        {
            bool result = _pool.TryGetValue(prefab, out var queue);
            if (!result || !queue.Any())
            {
                var newInstance = Object.Instantiate(prefab, parent);
                InitializeInstance(newInstance);
                return newInstance;
            }
            var item = queue.Dequeue();
            item.SetActive(true);
            item.transform.SetParent(parent);
            return item;
        }

        public void ReturnPoolObject(GameObject prefab, GameObject instance)
        {
            bool result = _pool.TryGetValue(prefab, out var queue);
            if (!result)
            {
                queue = new Queue<GameObject>();
                _pool.Add(prefab,queue);
            }
            queue.Enqueue(instance);
            instance.gameObject.SetActive(false);
            instance.transform.SetParent(PoolTransform);
        }

        private void InitializeInstance(GameObject instance)
        {
            var allInjectables = instance.GetComponentsInChildren<IInjectable>(true);
            foreach (var injectable in allInjectables)
            {
                injectable.Inject(_injectionContainer);
            }
            var allInitializables = instance.GetComponentsInChildren<IInitialize>(true);
            foreach (var initializable in allInitializables)
            {
                initializable.Initialize();
            }
        }
    }

    public static class InjectablePoolExtensions
    {
        public static T GetPoolObject<T>(this IInjectablePool pool, T prefab, Transform transform)
            where T : MonoBehaviour
        {
            return pool.GetPoolObject(prefab.gameObject, transform).GetComponent<T>();
        }
    }
}
namespace com.ats.mvc
{
    public interface ITrigger
    {
        IMediator Mediator { get; }
    }
}
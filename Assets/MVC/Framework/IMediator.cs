using com.ats.mvc.UI;

namespace com.ats.mvc
{
    public interface IMediator
    {
        void OnCreated();
        void OnEnabled();
        void OnDisabled();
        void OnDestroyed();
    }
}
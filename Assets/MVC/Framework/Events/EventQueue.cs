using System;
using System.Collections.Generic;
using UniRx;

namespace com.ats.mvc.Events
{
    internal class EventQueue<T>
    { 
        private readonly ICollection<Action<T>> _listeners = new List<Action<T>>();

        internal void AddListener(Action<T> listener)
        {
            _listeners.Add(listener);
        }

        internal void RemoveListener(Action<T> listener)
        {
            _listeners.Add(listener);
        }

        internal void Invoke(T data)
        {
            foreach (var listener in _listeners)
            {
                listener?.Invoke(data);
            }
        }

        internal IObservable<T> AsObservable()
        {
            return Observable.FromEvent<T>(AddListener, RemoveListener);
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;

namespace com.ats.mvc.Events
{
    public class EventBus
    {
        private Dictionary<Type, object> _events = new Dictionary<Type, object>();
        
        public void Publish<T>(T eventData)
        {
            GetQueue<T>().Invoke(eventData);
        }

        public void Subscribe<T>(Action<T> listener)
        {
            GetQueue<T>().AddListener(listener);
        }

        public void Unsubscribe<T>(Action<T> listener)
        {
            GetQueue<T>().RemoveListener(listener);
        }

        public IObservable<T> QueueAsObservable<T>()
        {
            return GetQueue<T>().AsObservable();
        }

        private EventQueue<T> GetQueue<T>()
        {
            var eventType = typeof(T);
            if (!_events.TryGetValue(eventType,out var queue))
            {
                queue = new EventQueue<T>();
                _events[eventType] = queue;    
            }
            return queue as EventQueue<T>;
        }
        
    }
}
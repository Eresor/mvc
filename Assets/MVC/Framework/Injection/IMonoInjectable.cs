using UnityEngine;

namespace com.ats.mvc.Injection
{
    public interface IMonoInjectable : IInjectable
    {
        Transform InjectionRoot { get; }
    }
}
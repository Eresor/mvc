using System;

namespace com.ats.mvc.Injection
{
    public interface IContext
    {
        void InstallBindings(IBinder binder);
    }
}
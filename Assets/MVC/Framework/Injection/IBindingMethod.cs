namespace com.ats.mvc.Injection
{
    interface IBindingMethod<T>
    {
        T GetInstance(object target);
    }
}
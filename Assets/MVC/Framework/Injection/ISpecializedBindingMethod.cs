namespace com.ats.mvc.Injection
{
    interface ISpecializedBindingMethod<TSource,TTarget>
    {
        TSource GetInstance(TTarget target);
    }
}
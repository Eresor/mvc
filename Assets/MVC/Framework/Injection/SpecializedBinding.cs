namespace com.ats.mvc.Injection
{
    public class SpecializedBinding<TSource, TTarget> : Binding<TSource>
    {
        internal ISpecializedBindingMethod<TSource, TTarget> SpecializedBindingMethod { get; set; }
    }
}
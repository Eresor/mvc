namespace com.ats.mvc.Injection
{
    public interface IInjectable
    {
        void Inject(IInjectionContainer binder);
    }
}
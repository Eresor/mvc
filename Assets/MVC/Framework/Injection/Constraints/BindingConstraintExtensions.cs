namespace com.ats.mvc.Injection.Constraints
{
    public static class BindingConstraintExtensions
    {
        public static SpecializedBinding<TSource,TTarget> WhenInjectedInto<TSource,TTarget>(this SpecializedBinding<TSource,TTarget> binding)
        {
            var constraint = new InjectIntoBindingConstraint<TTarget>();
            binding.Constraints.Add(constraint);
            return binding;
        }

        public static Binding WhenInjectedInto<TTarget>(this Binding binding)
        {
            var constraint = new InjectIntoBindingConstraint<TTarget>();
            binding.Constraints.Add(constraint);
            return binding;
        }
        
        public static Binding<TTarget> WithTag<TTarget,TTag>(this Binding<TTarget> binding, TTag tag)
        {
            var constraint = new InjectWithTagBindingConstraint<TTag>(tag);
            binding.Constraints.Add(constraint);
            return binding;
        }
    }
}
namespace com.ats.mvc.Injection.Constraints
{
    class InjectIntoBindingConstraint<T> : IBindingConstraint
    {
        public bool IsValid(object target, object tag)
        {
            return target is T;
        }
    }
}
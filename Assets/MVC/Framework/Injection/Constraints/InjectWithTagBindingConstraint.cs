namespace com.ats.mvc.Injection.Constraints
{
    public class InjectWithTagBindingConstraint<T> : IBindingConstraint
    {
        private readonly T _tag;

        public InjectWithTagBindingConstraint(T tag)
        {
            _tag = tag;
        }
        
        public bool IsValid(object target, object tag)
        {
            return tag is T typedTag && typedTag.Equals(_tag);
        }
    }
}
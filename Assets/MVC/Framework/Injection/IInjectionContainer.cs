namespace com.ats.mvc.Injection
{
    public interface IInjectionContainer
    {
        TInjected Get<TInjected,TContext>(TContext context);
        TInjected Get<TInjected,TContext,TTag>(TContext context, TTag tag);
        void InjectDependencies<TTarget>(TTarget target);
        void Initialize<TTarget>(TTarget target);
    }
}
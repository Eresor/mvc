using UnityEngine;

namespace com.ats.mvc.Injection
{
    public interface IInjectablePool
    {
        GameObject GetPoolObject(GameObject prefab, Transform parent);
        void ReturnPoolObject(GameObject prefab, GameObject instance);
    }
}
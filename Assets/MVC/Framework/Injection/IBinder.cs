namespace com.ats.mvc.Injection
{
    public interface IBinder
    {
        Binding<T> Bind<T>();

        SpecializedBinding<TSource, TTarget> Bind<TSource, TTarget>();
    }
}
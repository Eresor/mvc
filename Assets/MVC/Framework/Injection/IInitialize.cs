namespace com.ats.mvc.Injection
{
    public interface IInitialize
    {
        void Initialize();
    }
}
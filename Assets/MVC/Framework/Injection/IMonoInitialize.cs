using UnityEngine;

namespace com.ats.mvc.Injection
{
    public interface IMonoInitialize : IInitialize
    {
        Transform InjectionRoot { get; }
    }
}
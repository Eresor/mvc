﻿using System;
using UnityEngine;

namespace com.ats.mvc.Injection.Sources
{
    public class FromSelfBindingMethod<T> : IBindingMethod<T>
    {
        private readonly Func<GameObject, T> _getInstance;

        public FromSelfBindingMethod(Func<GameObject, T> getInstance)
        {
            _getInstance = getInstance;
        }

        public T GetInstance(object target)
        {
            var targetAsComponent = (target as Component);
            if (targetAsComponent != null)
            {
                return _getInstance.Invoke(targetAsComponent.gameObject);
            }
        
            var targetAsGameObject = (target as GameObject);
            if (targetAsGameObject != null)
            {
                return _getInstance.Invoke(targetAsGameObject);
            }
            return default;
        }
    }
}
using System;

namespace com.ats.mvc.Injection.Sources
{
    class FromOwnerBindingMethod<T,TOwner> : ISpecializedBindingMethod<T,TOwner>
    {
        private Func<TOwner, T> _func;

        public FromOwnerBindingMethod(Func<TOwner, T> func)
        {
            _func = func;
        }

        public T GetInstance(TOwner target)
        {
            return _func(target);
        }
    }
}
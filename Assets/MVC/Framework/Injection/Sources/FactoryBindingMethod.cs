using com.ats.mvc.Injection;

namespace com.ats.mvc.Injection.Sources
{
    class FactoryBindingMethod<T> : IBindingMethod<T>
    {
        private IFactory<T> _factory;

        public FactoryBindingMethod(IFactory<T> factory)
        {
            _factory = factory;
        }

        public T GetInstance(object target)
        {
            return _factory.Create();
        }
    }
}
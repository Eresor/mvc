using System;
using UnityEngine;

namespace com.ats.mvc.Injection.Sources
{
    public static class BindingFromExtensions
    {
        public static Binding<T> FromDefaultConstructor<T>(this Binding<T> binding) where T : new()
        {
            binding.BindingMethod = new DefaultConstructorBindingMethod<T>();
            return binding;
        }
        
        public static Binding<T> FromFactory<T>(this Binding<T> binding, IFactory<T> factory)
        {
            binding.BindingMethod = new FactoryBindingMethod<T>(factory);
            return binding;
        }

        public static Binding<T> FromInstance<T>(this Binding<T> binding, T instance)
        {
            binding.BindingMethod = new InstanceBindingMethod<T>(instance);
            return binding;
        }

        public static Binding<T> FromComponentOn<T>(this Binding<T> binding, GameObject gameObject)
        {
            binding.BindingMethod = new GetComponentBindingMethod<T>(gameObject);
            return binding;
        }
        
        public static Binding<T> FromComponentOnSelf<T>(this Binding<T> binding)
        {
            binding.BindingMethod = new GetComponentFromSelfBindingMethod<T>();
            return binding;
        }
        
        public static Binding<T> FromSelf<T>(this Binding<T> binding, Func<GameObject, T> getInstance)
        {
            binding.BindingMethod = new FromSelfBindingMethod<T>(getInstance);
            return binding;
        }

        public static Binding<T> FromDerivedType<T,TDerived>(this Binding<T> binding) where TDerived : T, new()
        {
            binding.BindingMethod = new DerivedTypeBindingMethod<T,TDerived>();
            return binding;
        }
    }
}
namespace com.ats.mvc.Injection.Sources
{
    class InstanceBindingMethod<T> : IBindingMethod<T>
    {
        private T _instance;

        public InstanceBindingMethod(T instance)
        {
            _instance = instance;
        }

        public T GetInstance(object target)
        {
            return _instance;
        }
    }
}
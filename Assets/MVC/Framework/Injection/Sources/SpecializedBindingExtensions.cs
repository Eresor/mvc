
using System;

namespace com.ats.mvc.Injection.Sources
{
    public static class SpecializedBindingExtensions
    {
        public static SpecializedBinding<TSource,TOwner> FromOwner<TSource, TOwner>(this SpecializedBinding<TSource,TOwner> binding, Func<TOwner, TSource> func)
        {
            binding.SpecializedBindingMethod = new FromOwnerBindingMethod<TSource,TOwner>(func);
            return binding;
        }
    }
}
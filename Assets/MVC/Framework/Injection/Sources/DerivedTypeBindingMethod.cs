namespace com.ats.mvc.Injection.Sources
{
    class DerivedTypeBindingMethod<TBase,TDerived> : IBindingMethod<TBase> where TDerived : TBase, new()
    {
        public TBase GetInstance(object target)
        {
            return new TDerived();
        }
    }
}
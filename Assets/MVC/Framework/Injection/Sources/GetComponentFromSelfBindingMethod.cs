using UnityEngine;

namespace com.ats.mvc.Injection.Sources
{
    //todo change to ISpecializedBinding
    class GetComponentFromSelfBindingMethod<T> : IBindingMethod<T>
    {
        public T GetInstance(object target)
        {
            var targetAsComponent = (target as Component);
            if (targetAsComponent != null)
            {
                return targetAsComponent.GetComponent<T>();
            }
            
            var targetAsGameObject = (target as GameObject);
            if (targetAsGameObject != null)
            {
                return targetAsGameObject.GetComponent<T>();
                
            }
            return default;
        }
    }
}
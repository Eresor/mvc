using com.ats.mvc.Injection;

namespace com.ats.mvc.Injection.Sources
{
    class DefaultConstructorBindingMethod<T> : IBindingMethod<T> where T : new()
    {
        public T GetInstance(object target)
        {
            return new T();
        }
    }
}
using com.ats.mvc.Injection;
using UnityEngine;

namespace com.ats.mvc.Injection.Sources
{
    class GetComponentBindingMethod<T> : IBindingMethod<T>
    {
        private GameObject _source;
        
        public T GetInstance(object target)
        {
            return _source.GetComponent<T>();
        }

        public GetComponentBindingMethod(GameObject source)
        {
            _source = source;
        }
    }
}
namespace com.ats.mvc.Injection
{
    public interface IFactory<T>
    {
        T Create();
    }
}
using System;
using System.Collections.Generic;

namespace com.ats.mvc.Injection
{
    public class Binding<TSource> : Binding
    {
        internal IBindingMethod<TSource> BindingMethod { get; set; }

        public Binding() 
            : base(typeof(TSource))
        {
            
        }
    }

    public class Binding
    {
        internal ICollection<IBindingConstraint> Constraints = new List<IBindingConstraint>();
        
        private Type _sourceType;

        internal Binding(Type sourceType)
        {
            this._sourceType = sourceType;
        }
    }
}
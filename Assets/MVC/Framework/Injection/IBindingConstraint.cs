namespace com.ats.mvc.Injection
{
    interface IBindingConstraint
    {
        bool IsValid(object target, object tag);
    }
}
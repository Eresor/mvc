using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace com.ats.mvc.Injection
{
    class BindingsContainer : IBinder, IInjectionContainer
    {
        Dictionary<Type, ICollection<Binding>> _bindings = new Dictionary<Type, ICollection<Binding>>();

        HashSet<int> _injectedSet = new HashSet<int>();
        HashSet<int> _initializedSet = new HashSet<int>();
        
        public Binding<T> Bind<T>()
        {
            var binding = new Binding<T>();
            var type = typeof(T);


            ICollection<Binding> typeBindings;
            if (!_bindings.TryGetValue(type, out typeBindings))
            {
                typeBindings = new List<Binding>();
                _bindings[type] = typeBindings;
            }

            typeBindings.Add(binding);

            return binding;
        }

        public SpecializedBinding<TSource, TTarget> Bind<TSource, TTarget>()
        {
            var binding = new SpecializedBinding<TSource, TTarget>();
            var type = typeof(TSource);


            ICollection<Binding> typeBindings;
            if (!_bindings.TryGetValue(type, out typeBindings))
            {
                typeBindings = new List<Binding>();
                _bindings[type] = typeBindings;
            }

            typeBindings.Add(binding);

            return binding;
        }

        public TInjected Get<TInjected, TTarget>(TTarget context)
        {
            return Get<TInjected, TTarget, object>(context, null);
        }

        public TInjected Get<TInjected, TTarget, TTag>(TTarget context, TTag tag)
        {
            var type = typeof(TInjected);
            TInjected instance = default;
            if (!_bindings.ContainsKey(type))
            {
                Debug.LogError($"Missing type {type} when injected to {context.GetType()}");
            }
            if (_bindings[type]
                    .FirstOrDefault(b =>
                        b is SpecializedBinding<TInjected, TTarget> bind &&
                        bind.Constraints.All(c => c.IsValid(context,tag))) is
                SpecializedBinding<TInjected, TTarget> binding)
            {
                instance = binding.SpecializedBindingMethod.GetInstance(context);
            }
            else
            {
                instance = Get<TInjected>(context,tag);
            }
            
            InjectDependencies(instance);
            Initialize(instance);
            return instance;
        }

        public void InjectDependencies<TTarget>(TTarget instance)
        {
            if (instance is IMonoInjectable monoInjectable)
            {
                var allInjectables = monoInjectable.InjectionRoot.GetComponentsInChildren<IInjectable>(true);
                foreach (var injectable in allInjectables)
                {
                    var hash = injectable.GetHashCode();
                    if (_injectedSet.Contains(hash))
                    {
                        continue;
                    }

                    _injectedSet.Add(hash);
                    injectable.Inject(this);
                }
            }
            else if (instance is IInjectable injectable)
            {
                var hash = injectable.GetHashCode();
                if (!_injectedSet.Contains(hash))
                {
                    _injectedSet.Add(hash);
                    injectable.Inject(this);
                }
            }
        }

        public void Initialize<TTarget>(TTarget instance)
        {
            if (instance is IMonoInitialize monoInitialize)
            {
                var allInitializables = monoInitialize.InjectionRoot.GetComponentsInChildren<IInitialize>(true);
                foreach (var initializable in allInitializables)
                {
                    var hash = initializable.GetHashCode();
                    if (_initializedSet.Contains(hash))
                    {
                        continue;
                    }

                    _initializedSet.Add(hash);
                    initializable.Initialize();
                }
            }
            else if(instance is IInitialize initialize)
            {
                var hash = instance.GetHashCode();
                if (!_initializedSet.Contains(hash))
                {
                    _initializedSet.Add(hash);
                    initialize.Initialize();
                }
            }
        }

        private T Get<T>(object target, object tag)
        {
            var type = typeof(T);
            return _bindings[type]
                    .FirstOrDefault(b => b is Binding<T> bind && bind.Constraints.All(c => c.IsValid(target,tag))) is
                Binding<T>
                binding
                ? binding.BindingMethod.GetInstance(target)
                : default;
        }
    }
}
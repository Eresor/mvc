using System;
using UnityEngine;

namespace com.ats.mvc.UI
{
    public interface IWindow : IView
    {
        Canvas Canvas { get; }
        CanvasGroup CanvasGroup { get; }
        RectTransform Root { get; }
        IWindowAnimation ShowAnimation { get; }
        IWindowAnimation HideAnimation { get; }
    }
}
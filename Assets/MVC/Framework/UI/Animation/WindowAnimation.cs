using System;
// using AnimeRx;
using UniRx;
using UnityEngine;

namespace com.ats.mvc.UI.Animation
{
    [Serializable]
    public class WindowAnimation : IWindowAnimation
    {
        [SerializeField] private float time;
        
        public void Play(IWindow window)
        {
            // Anime.Play(0, 1, Easing.Linear(time)).DoOnCompleted(AnimationCompletedHandler)
            //     .Subscribe(alpha => window.CanvasGroup.alpha = alpha);
            AnimationStarted?.Invoke();
            AnimationCompletedHandler();
        }

        private void AnimationCompletedHandler()
        {
            AnimationEnded?.Invoke();
        }

        public event Action AnimationStarted;
        public event Action AnimationEnded;
    }
}
namespace com.ats.mvc.UI
{
    public interface IWindowIdentifier
    {
        string Identifier { get; }
    }
}
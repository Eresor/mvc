using UnityEngine;

namespace com.ats.mvc.UI
{
    public interface IUiNavigation
    {
        IWindowNavigationHandler GetNavigationHandler(IWindowIdentifier identifier);
    }
}
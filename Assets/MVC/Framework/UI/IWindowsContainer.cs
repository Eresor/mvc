using com.ats.mvc.Injection;

namespace com.ats.mvc.UI
{
    public interface IWindowsContainer
    {
        IWindow GetWindow(IWindowIdentifier identifier);
    }
}
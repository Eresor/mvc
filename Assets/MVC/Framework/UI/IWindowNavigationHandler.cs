using System;

namespace com.ats.mvc.UI
{
    public interface IWindowNavigationHandler : IDisposable
    {
        void Show();
        void Hide();
    }
}
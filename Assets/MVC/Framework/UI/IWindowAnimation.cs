using System;

namespace com.ats.mvc.UI
{
    public interface IWindowAnimation
    {
        void Play(IWindow window);
        event Action AnimationStarted;
        event Action AnimationEnded;
    }
}
using System;
using com.ats.mvc.Injection;
using com.ats.mvc.UI.Implementation;
using UniRx;
using UnityEngine;

namespace com.ats.mvc.UI
{
    public class WindowNavigationHandler : IWindowNavigationHandler
    {
        private readonly RectTransform _uiRootTransform;
        private CompositeDisposable _disposables = new CompositeDisposable();
        private readonly IWindow _window = null;

        public WindowNavigationHandler(IWindow window, RectTransform uiRootTransform)
        {
            _uiRootTransform = uiRootTransform;
            _window = window;
        }
        
        public void Show()
        {
            if (_window.Root.parent != _uiRootTransform)
            {
                _window.Root.SetParent(_uiRootTransform);
            }
            _window.Canvas.enabled = true;
            _window.ShowAnimation.Play(_window);
        }

        public void Hide()
        {
            Observable.FromEvent(h => _window.HideAnimation.AnimationEnded += h,
                    h => _window.HideAnimation.AnimationEnded -= h)
                .First()
                .Subscribe(_ => { _window.Canvas.enabled = false; })
                .AddTo(_disposables);
            _window.HideAnimation.Play(_window);
        }

        public void Dispose()
        {
            _disposables?.Dispose();
        }
    }
}
using System;
using com.ats.mvc.Injection;
using com.ats.mvc.UI.Animation;
using UnityEngine;
using Zenject;

namespace com.ats.mvc.UI.Implementation
{
    [RequireComponent(typeof(CanvasGroup))]
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(RectTransform))]
    public abstract class Window : MonoBehaviour, IWindow, IMonoInjectable
    {
        [SerializeField] private WindowAnimation showAnimation;
        [SerializeField] private WindowAnimation hideAnimation;
        
        public void Inject(IInjectionContainer container)
        {
            Canvas = container.Get<Canvas,Window>(this);
            CanvasGroup = container.Get<CanvasGroup,Window>(this);
            Root = container.Get<RectTransform,Window>(this);
        }

        public Canvas Canvas { get; private set; }
        public CanvasGroup CanvasGroup { get; private set; }
        public RectTransform Root  { get; private set; }

        public IWindowAnimation ShowAnimation => showAnimation;
        public IWindowAnimation HideAnimation => hideAnimation;
        

        public abstract void AddListeners();
        public abstract void RemoveListeners();
        public Transform InjectionRoot => transform;
    }
}
using com.ats.mvc.Injection;
using UnityEngine;

namespace com.ats.mvc.UI.Implementation
{
    public class UiNavigation : IUiNavigation, IInjectable
    {
        private IWindowsContainer _windowsContainer = null;
        private RectTransform _uiRootTransform = null;

        public IWindowNavigationHandler GetNavigationHandler(IWindowIdentifier identifier)
        {
            var window = _windowsContainer.GetWindow(identifier);
            return new WindowNavigationHandler(window, _uiRootTransform);
        }

        public void Inject(IInjectionContainer binder)
        {
            _windowsContainer = binder.Get<IWindowsContainer, IUiNavigation>(this);
            _uiRootTransform = binder.Get<RectTransform, IUiNavigation>(this);
        }
    }
}
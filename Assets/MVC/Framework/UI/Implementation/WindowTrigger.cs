using System;
using com.ats.mvc.Injection;
using UnityEngine;
using Zenject;

namespace com.ats.mvc.UI.Implementation
{
    public class WindowTrigger : MonoBehaviour, ITrigger, IInjectable, IInitialize
    {
        private Window Window { get; set; }
        public IMediator Mediator { get; private set; }
        
        public void Inject(IInjectionContainer binder)
        {
            Mediator = binder.Get<IMediator,WindowTrigger>(this);
            Window = binder.Get<Window,WindowTrigger>(this);

            Window.ShowAnimation.AnimationStarted += ShowStartedHandler;
            Window.HideAnimation.AnimationEnded += HideEndedHandler;
        }

        private void HideEndedHandler()
        {
            Mediator.OnDisabled();
        }

        private void ShowStartedHandler()
        {
            Mediator.OnEnabled();
        }

        public void Initialize()
        {
            Mediator.OnEnabled();
        }

        private void OnDestroy()
        {
            Window.ShowAnimation.AnimationStarted -= ShowStartedHandler;
            Window.HideAnimation.AnimationEnded -= HideEndedHandler;
            Mediator.OnDestroyed();
        }
    }
}
using com.ats.mvc;
using com.ats.mvc.Events;
using com.ats.mvc.Injection;
using UnityEngine;

namespace MVC.UI.Implementation
{
    public class ViewMediator<TView> : MonoBehaviour, IInjectable where TView : IView
    {
        protected TView View { get; private set; }
        protected EventBus EventBus { get; private set; }

        public virtual void Inject(IInjectionContainer binder)
        {
            EventBus = binder.Get<EventBus, ViewMediator<TView>>(this);
            View = binder.Get<TView, ViewMediator<TView>>(this);
        }
    }
    
    public abstract class ViewMediator<TView, TListener> : ViewMediator<TView>  where TView : IView
    {
        protected TListener Listener { get; private set; }
        
        public override void Inject(IInjectionContainer binder)
        {
            base.Inject(binder);
            Listener = binder.Get<TListener,ViewMediator<TView,TListener>>(this);
            
            SetListener(Listener);
        }

        protected abstract void SetListener(TListener listener);
    }
}
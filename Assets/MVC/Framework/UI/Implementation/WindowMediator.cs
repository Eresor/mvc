using System;
using com.ats.mvc.Events;
using com.ats.mvc.Injection;
using UnityEngine;

namespace com.ats.mvc.UI.Implementation
{
    public abstract class WindowMediator<TWindow> : MonoBehaviour, IInjectable where TWindow : IWindow
    {
        protected TWindow Window { get; private set; }
        protected EventBus EventBus { get; private set; }

        public virtual void Inject(IInjectionContainer binder)
        {
            Window = binder.Get<TWindow,WindowMediator<TWindow>>(this);
            EventBus = binder.Get<EventBus, WindowMediator<TWindow>>(this);
        }
    }

    public abstract class WindowMediator<TWindow, TListener> : WindowMediator<TWindow> where TWindow : IWindow
    {
        protected TListener Listener { get; private set; }
        public override void Inject(IInjectionContainer binder)
        {
            base.Inject(binder);
            Listener = binder.Get<TListener,WindowMediator<TWindow,TListener>>(this);
        }
    }
}